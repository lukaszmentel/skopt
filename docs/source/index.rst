.. SKOPT documentation master file, created by
   sphinx-quickstart on Tue Dec 27 01:30:55 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SKOPT's documentation!
=================================

SKOPT is a parameter-optimisation framework for the 
Density Functional Tight-Binding (DFTB) theory.

.. _Python: http://www.python.org
.. _`DFTB+`: http://www.dftb-plus.info/
.. _Lodestar: http://yangtze.hku.hk/new/software.php
.. _dftb.org: http://www.dftb.org/home/
.. _`MIT license`: https://opensource.org/licenses/MIT


News
====
   
   * SKOPT version 0.1.0 released: February 2017.

Contents
==================

.. toctree::
   :maxdepth: 2

   about
   install
   commands
   tutorials/tutorials
   reference
   modules
   license
   develop
   contributors


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

