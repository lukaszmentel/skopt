.. index:: core

.. _skopt.core:

core sub-package modules
===========================================================

Main Program (skopt.core.skopt)
-------------------------------------

.. automodule:: skopt.core.skopt
    :members:
    :undoc-members:
    :show-inheritance:


Input handler (skopt.core.input)
----------------------------------

.. automodule:: skopt.core.input
    :members:
    :undoc-members:
..    :show-inheritance:


Tasks (skopt.core.tasks)
-------------------------

.. automodule:: skopt.core.tasks
    :members:
    :undoc-members:
    :show-inheritance:


Task Dictionary (skopt.core.taskdict)
-----------------------------------------

.. automodule:: skopt.core.taskdict
    :members:
    :undoc-members:
..    :show-inheritance:


Objectives (skopt.core.objectives)
-----------------------------------

.. automodule:: skopt.core.objectives
    :members:
    :undoc-members:
    :show-inheritance:


Query (skopt.core.query)
-------------------------

.. automodule:: skopt.core.query
    :members:
    :undoc-members:
    :show-inheritance:


Evaluator (skopt.core.evaluate)
---------------------------------

.. automodule:: skopt.core.evaluate
    :members:
    :undoc-members:
    :show-inheritance:

Optimiser (skopt.core.optimise)
---------------------------------

.. automodule:: skopt.core.optimise
    :members:
    :undoc-members:
    :show-inheritance:

Parameters (skopt.core.parameters)
-----------------------------------

.. automodule:: skopt.core.parameters
    :members:
    :undoc-members:
    :show-inheritance:

.. _`pso`:

PSO (skopt.core.pso)
------------------------------

.. automodule:: skopt.core.pso
    :members:
    :undoc-members:
..    :show-inheritance:

Utilities (skopt.core.utils)
------------------------------

.. automodule:: skopt.core.utils
    :members:
    :undoc-members:
..    :show-inheritance:
