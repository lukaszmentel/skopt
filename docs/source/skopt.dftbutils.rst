.. index:: dftbutils

.. _skopt.dftbutils:

dftbutils sub-package
=====================================================

Run BS (skopt.dftbutils.runBS)
-------------------------------

.. automodule:: skopt.dftbutils.runBS
    :members:
    :undoc-members:
..    :show-inheritance:

Query DFTB (skopt.dftbutils.queryDFTB)
------------------------------------------

.. automodule:: skopt.dftbutils.queryDFTB
    :members:
    :undoc-members:
..    :show-inheritance:

Query k-Lines (skopt.dftbutils.querykLines)
--------------------------------------------

.. automodule:: skopt.dftbutils.querykLines
    :members:
    :undoc-members:
..    :show-inheritance:

Lattice (skopt.dftbutils.lattice)
---------------------------------------

.. automodule:: skopt.dftbutils.lattice
    :members:
    :undoc-members:
..    :show-inheritance:

Plot (skopt.dftbutils.plot)
-----------------------------

.. automodule:: skopt.dftbutils.plot
    :members:
    :undoc-members:
..    :show-inheritance:

Unils (skopt.dftbutils.utils)
-------------------------------

.. automodule:: skopt.dftbutils.utils
    :members:
    :undoc-members:
..    :show-inheritance:
