"""
Assumptions for the reference data:
Reference data comes from publications, where E-k pairs are quoted typically using crystallographic 
notation for the k-points, rather than fractions of $2\pi$ over a lattice constant. 
It convenes therefore, to store them as a dictionary. Some translation is necessary when evaluation 
and visualisation is done, since each dictionary key (k-point name) must become a k-point index or 
a value in $[m^{-1}]$, but this translation is specific to the tool used to calculate the band-structure; 
hence the translation will happen elsewhere in the code.  
Additional reference data may include effective masses or location of band extrema, and explicit band-gaps 
(e.g. $E_{c,min}-E_{v,max}$).
"""
# Reference E-k points for Si, as cited in 
# Semiconductors: Group IV Elements and III-V Compounds, ed. O. Madelung (Springer, New York, 1991).; 
# Crystallographic (k-) points vs. energy in [eV]
refEk = {'G25pr_v': 0.0,    # Top of the valence band -- energy reference, 2-fold degeneracy (lh,hh)
         'Gso_v': -0.045,   # VB Split-off at k=0
         'G1_v': -12.5,     # -12.4 exp, -12.5 exp, -12.36 th, -12.34 th
         'G15_c': 3.35,     # (3.34..3.36 exp, 3.42 th, 3.5 th)
         'G2pr_c': 4.15,    # (4.09 th, 4.15 exp, 4.21 exp)
         'X1_c': 1.35,      # cardona?
         'X4_v': -2.9,      # (-2.89 theory, -2.9 exp, -2.5 exp)
         'X1_v': -8.01,     # (-7.71 th, -7.75 th)
         'L3_c': 4.15,      # (3.9 exp, 4.15 exp, 4.33 th, 4.34 th)
         'L1_c': 2.4,       # (2.26 th, 2.29 th, 2.04 exp, 2.4 exp)
         'L3pr_v': -1.20,    # (-1.2 exp, -1.24 th, -1.25 th)
         'L1_v': -7.0,      # (-6.99 th, -7.01 th, -6.8 exp, -6.4 exp)
         'L2pr_v': -9.3,    # (-9.3 exp, -9.59 th, -9.62 th)
         'K1_v2': -8.15,    # (-8.1, -8.15 th)
         'K3_v': -7.16,     # (-7.11, -7.16 th)
         'K1_v1': -4.37,    # (-4.37, -4.41 th)
         'K2_v': -2.47,     # (-2.45, -2.47 th)
         'K3_c1': 1.7,      # (1.7 th)
         'K3_c2': 4.79,     # (4.61, 4.79 th)
         }

refEgap = {'Egap': 1.12,}   # [eV], fundamental band-gap
           
refEk_st = {'Dmin_c': 1.12,     # CB min, 85% along Gamma-X (Delta direction)
            'Dmin_c_pos': 0.85, # relative position of the Delta min of CB, along Gamma-X
            'Smin_v': -4.4,     # [eV], (-4.4 th, -4.4 exp, -4.7 exp), VB[2] min along K-Gamma (Sigma direction)
           }

refmeff = {'me_Xl': 0.916, 
           'me_Xt': 0.190, # CB meff for the Delta-valley
           'm_lh_001': -0.204, 
           'm_lh_110': -0.147,
           'm_lh_111': -0.139,
           'm_hh_001': -0.276, 
           'm_hh_110': -0.579,
           'm_hh_111': -0.738,
           'm_so_001': -0.234,
           'm_so_111': -0.234,
           'm_so_110': -0.234,
           'm_so_av':  -0.234, # SO masses should be identical
           'm_so_sd':  0.,     # and their standard deviation should be 0
          }


ref_Si = dict(list(refEk.items()) + list(refEgap.items()) + 
                list(refEk_st.items()) + list(refmeff.items()))
